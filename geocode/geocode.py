from flask import Flask, request, Response, stream_with_context
import requests
import logging


app = Flask(__name__)
logging.basicConfig(level=logging.INFO)
LOG = logging.getLogger("geocode.py")


@app.route('/geocode')
def proxy():
    r = fetch_geocode()
    LOG.info("Got %s response", r.status_code)
    return Response(stream_with_context(r.iter_content()),
                    content_type=r.headers['content-type'])


def fetch_geocode():
    payload = {'api_key': request.headers['X-API-Key'],
               'query': request.args['query'],
               'search_center': request.args['search_center'],
               'type': 'address-only'}
    return requests.get('https://apis.location.studio/geo/v2/search/json',
                        params=payload)
